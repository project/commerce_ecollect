<?php

/**
 * @file
 * Template file to display help info.
 */

?>
<div class="commerce-ecollect-help-info">
  <div class="cehi-message">
    <h2> <?php print t('About this module'); ?> </h2>
      <p>
        <?php
          print t('<a href="@ecollect-link">eCollect</a> is a colombian payment gateway. With this module,
            you can integrate your store with it, because it is based on rules that define payment parameters.
            Useful to change those parameters depending on several conditions.',
            array('@ecollect-link' => 'http://www.avisortech.com')
          );
        ?>
      </p>
    <h2><?php print t('Installation'); ?> </h2>
      <?php
        $enable_content = t('Download the NuSOAP library from <a href="@nusoap-dl">this link</a>,
          and place it inside <em>sites/all/libraries/nusoap</em>. You should see a PHP classs
          <em>sites/all/libraries/nusoap/lib/nusoap.php</em>.',
           array('@nusoap-dl' => 'http://sourceforge.net/projects/nusoap/files/latest/download')
        );
      ?>
      <ul>
        <li><?php print($enable_content); ?></li>
        <li><?php print(t('Enable the module.')); ?></li>
      </ul>
    <h2> <?php print(t('Usage')); ?> </h2>
      <p>
        <?php
          print(t('When module is enabled, you need to add a custom rule to set right payment parameters.
            Go to <a href="@rules-page">admin/config/workflow/rules</a>, and create a new with your desired react
            on event. You can set conditions, if you need. Now, add an action <em>Set payment parameters</em>,
            which may be located inside group <em>ECollect</em>.
            Keep in mind that some of these field values must be asked to Avisor Technologies.',
            array('@rules-page' => url('admin/config/workflow/rules')))
          );
        ?>
      </p>
    <h2><?php print(t('Why do we use Rules to set payment parameters?')); ?> </h2>
      <p>
        <?php
          print(t('That is simple. Avisor gives you the possibility to have several "payment pages" as you want.
            For example, you can setup a page to pay events with following fields: <em>Full name, Personal ID,
            Phone</em>. Also, a different page to pay courses with following fields: <em>Full name, Personal ID,
            Phone, Customer ID, Course ID</em>. Therefore, you need to setup different values for each one.
            That is the trick!. Also (not tested yet) if you have a "multi-store website", you could setup different values per shop.'));
          ?>
      </p>
  </div>
</div>
