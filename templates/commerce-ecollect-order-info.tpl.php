<?php

/**
 * @file
 * Template file to display order status message.
 */

?>
<div class="commerce-ecollect-order-info">
  <div class="ceoi-message">
    <?php print $data['message']; ?>
  </div>
</div>
